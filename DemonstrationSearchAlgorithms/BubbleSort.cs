﻿namespace DemonstrationSearchAlgorithms
{
  public static class BubbleSort
  {
    public static void Sort(int[] array)
    {
      var smallest = -1;
      var largest = array.Length;

      while (smallest < largest)
      {
        var index = smallest + 2;
        while (index < largest)
        {
          if (array[index - 1] > array[index])
          {
            Swap(index - 1, index, array);
          }
          ++index;
        }
        --largest;

        index = largest - 2;
        while (index > smallest)
        {
          if (array[index] > array[index + 1])
          {
            Swap(index, index + 1, array);
          }
          --index;
        }
        ++smallest;
      }
    }

    private static void Swap(int first, int second, int[] array)
    {
      (array[first], array[second]) = (array[second], array[first]);
    }
  }
}