﻿namespace DemonstrationSearchAlgorithms.Search
{
  public class Linear : Base
  {
    public override string Name => "Linear search";

    public override int FindIndex(int[] array, int element)
    {
      for (int i = 0; i < array.Length; i++)
      {
        if (array[i] == element)
          return i;

        if (array[i] > element)
          return ElementNotFind;
      }
      return ElementNotFind;
    }
  }
}