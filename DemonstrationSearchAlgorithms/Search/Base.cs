﻿namespace DemonstrationSearchAlgorithms.Search
{
  public abstract class Base
  {
    public const int ElementNotFind = -1;
    
    public abstract string Name { get; }

    public abstract int FindIndex(int[] array, int element);
  }
}