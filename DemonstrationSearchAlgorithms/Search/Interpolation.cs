﻿namespace DemonstrationSearchAlgorithms.Search
{
  public class Interpolation : Base
  {
    public override string Name => "Interpolation search";
    
    public override int FindIndex(int[] array, int element)
    {
      var min = 0;
      var max = array.Length - 1;
      while (element >= array[min] && element <= array[max] && array[max] != array[min])
      {
        var middle = min + (max - min) * ((element - array[min]) / (array[max] - array[min]));

        if (array[middle] == element)
          return middle;

        if (array[middle] > element)
        {
          max = middle - 1;
        }
        else if (array[middle] < element)
        {
          min = middle + 1;
        }
      }
      
      return ElementNotFind;
    }
  }
}