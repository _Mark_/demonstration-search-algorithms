﻿namespace DemonstrationSearchAlgorithms.Search
{
  public class Binary : Base
  {
    public override string Name => "Binary search";
    
    public override int FindIndex(int[] array, int element)
    {
      var min = 0;
      var max = array.Length - 1;
      while (min <= max)
      {
        var middle = (min + max) / 2;
        if (element > array[middle])
          min = middle + 1;
        else if (element < array[middle])
          max = middle - 1;
        else return middle;
      }
      return ElementNotFind;
    }
  }
}