﻿using System;
using static System.Console;

namespace DemonstrationSearchAlgorithms
{
  class Program
  {
    private const int _minValue = 10;
    private const int _maxValue = 99;
    private const int _arraySize = 100;
    private static readonly Random _random = new Random();

    static void Main(string[] args)
    {
      Search.Base[] search =
      {
        new Search.Linear(),
        new Search.Binary(),
        new Search.Interpolation()
      };

      for (int i = 0; i < search.Length; i++)
      {
        WriteLine($"{search[i].Name}:");
        int[] array = CreateSortedArray();
        DisplayArray(array);

        var randomElement = _random.Next(_minValue, _maxValue + 1);
        WriteLine($"Element for search: {randomElement}");

        int index = search[i].FindIndex(array, randomElement);
        if (index == Search.Base.ElementNotFind)
        {
          WriteLine("Element not find.");
        }
        else
        {
          WriteLine($"{randomElement} with index: {index}");
        }

        WriteLine();
        WriteLine();
      }

      ReadLine();
    }

    private static void DisplayArray(int[] array)
    {
      for (int i = 0; i < array.Length; i++)
      {
        Write($"{i}:{array[i]} ");
      }
      WriteLine();
      WriteLine();
    }

    private static int[] CreateSortedArray()
    {
      var array = CreteRandomArray();

      BubbleSort.Sort(array);
      return array;
    }

    private static int[] CreteRandomArray()
    {
      int[] array = new int[_arraySize];
      for (int i = 0; i < array.Length; i++)
      {
        array[i] = _random.Next(_minValue, _maxValue + 1);
      }
      return array;
    }
  }
}